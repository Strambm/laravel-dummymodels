<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cetria\Laravel\Helpers\Test\Dummy\Factory\ComplaintFactory;

class Complaint extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'date',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'date' => 'date',
        'workspace_id' => 'integer',
    ];

    public static function factory()
    {
        return ComplaintFactory::new();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function basketItems(): MorphMany
    {
        return $this->morphMany(Basket::class, 'object');
    }
}
