<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\Factory;

use function rand;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    use SoftDeletes;
    
    protected $model = Product::class;
    
    public function definition(): array
    {
        return [
            'name' => $this->faker->word(),
            'url' => $this->faker->url(),
            'price' => rand(10, 10000),
        ];
    }

    public function uniqueName(): ProductFactory
    {
        return $this->state(function(array $attributes): array {
            return [
                'name' => $this->faker->unique()->word(),
            ];
        });
    }
}
