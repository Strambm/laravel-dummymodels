<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\Factory;

use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{   
    protected $model = Category::class;
    
    public function definition(): array
    {

        return [
            'name' => $this->faker->word(),
        ];
    }
}
