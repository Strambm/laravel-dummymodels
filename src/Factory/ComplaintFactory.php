<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\Factory;

use function date;
use function rand;
use Cetria\Laravel\Helpers\Test\Dummy\Complaint;
use Cetria\Laravel\Helpers\Test\Dummy\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComplaintFactory extends Factory
{
    use SoftDeletes;
    
    protected $model = Complaint::class;
    
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'date' => date("Y-m-d", rand(1262055681,1262055681)),
        ];
    }
}
