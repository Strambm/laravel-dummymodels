<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\Factory;

use function date;
use function rand;
use Cetria\Laravel\Helpers\Test\Dummy\User;
use Cetria\Laravel\Helpers\Test\Dummy\Order;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    use SoftDeletes;
    
    protected $model = Order::class;
    
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'date' => date("Y-m-d", rand(1262055681,1262055681)),
        ];
    }
}
