<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\Factory;

use function rand;
use Cetria\Laravel\Helpers\Test\Dummy\Order;
use Cetria\Laravel\Helpers\Test\Dummy\Basket;
use Cetria\Laravel\Helpers\Test\Dummy\Complaint;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class BasketFactory extends Factory
{
    use SoftDeletes;
    
    protected $model = Basket::class;
    
    public function definition(): array
    {

        return [
            'price' => rand(10, 10000),
            'product_id' => Product::factory(),
            'amount' => rand(1, 10),
            'object_id' => Order::factory(),
            'object_type' => Order::class,
        ];
    }

    public function order(): Factory
    {
        return $this->state(function(): array
        {
            return [
                'object_id' => Order::factory(),
                'object_type' => Order::class,
            ];
        });
    }

    public function complaint(): Factory
    {
        return $this->state(function(): array
        {
            return [
                'object_id' => Complaint::factory(),
                'object_type' => Complaint::class,
            ];
        });
    }
}
