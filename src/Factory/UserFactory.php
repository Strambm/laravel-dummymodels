<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\Factory;

use Cetria\Laravel\Helpers\Test\Dummy\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    use SoftDeletes;
    
    protected $model = User::class;
    
    public function definition(): array
    {
        return [
            'name' => \uniqid('name id: '),
        ];
    }
}
