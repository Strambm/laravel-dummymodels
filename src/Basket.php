<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Cetria\Laravel\Helpers\Test\Dummy\Factory\BasketFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Basket extends Model
{
    use HasFactory;

    protected $fillable = [
        'price',
        'product_id',
        'amount',
        'object_id',
        'object_type',
    ];

    protected $casts = [
        'price' => 'integer',
        'product_id' => 'string',
        'amount' => 'integer',
        'object_id' => 'integer',
        'object_type' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    public static function factory()
    {
        return BasketFactory::new();
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function document(): MorphTo
    {
        return $this->morphTo();
    }

    public function complaint(): BelongsTo
    {
        return $this->belongsTo(Complaint::class);
    }
}
