<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Cetria\Laravel\Helpers\Test\Dummy\Factory\CategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = 'categories';
    public $timestamps = false;

    public static function factory()
    {
        return CategoryFactory::new();
    }

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
    ];

    
}
