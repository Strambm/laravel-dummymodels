<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Cetria\Laravel\Helpers\Test\Dummy\DatabaseDrivers\Driver;
use Cetria\Laravel\Helpers\Test\Dummy\DatabaseDrivers\Sqlite;
use Mockery;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Builder;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Factories\Factory;

class DummyHelper
{
    private static $db;

    public static function init(
        ?Driver $databaseDriver = null,
        string $fakerProvider = 'en_US'
    ): void
    {
        $databaseDriver = static::getDatabaseDriver($databaseDriver);
        $container = Container::getInstance();
        $container->singleton(Generator::class, function ($app, $parameters) use ($fakerProvider) {
            return \Faker\Factory::create($fakerProvider);
        });
        $container->instance(Application::class, $app = Mockery::mock(Application::class));
        $app->shouldReceive('getNamespace')->andReturn('App\\');
        $databaseDriver->setConnection();
        Factory::useNamespace(\Cetria\Laravel\Helpers\Test\Dummy\Factory::class . '\\');
        static::migrate();
        static::$db = $databaseDriver;
    }

    private static function getDatabaseDriver(?Driver $databaseDriver): Driver
    {
        if(is_null($databaseDriver)) {
            $databaseDriver = new Sqlite();
        }
        return $databaseDriver;
    }

    private static function migrate(): void
    {
        $classes = [
            Category::class,
            Basket::class,
            Complaint::class,
            Order::class,
            Product::class,
            User::class
        ];
        foreach($classes as $class) {
            static::migrateForTable(new $class());
        }

        $builder = static::getSchemaBuilder();
        $builder->create(
            'category_product',
            function(Blueprint $table): void
            {
                $table->integer('product_id');
                $table->integer('category_id');
            }
        );
    }

    private static function migrateForTable(Model $instance)
    {
        $builder = static::getSchemaBuilder();
        $builder->create(
            $instance->getTable(), 
            function(Blueprint $table) use ($instance): void 
            {
                $table->increments('id');
                foreach($instance->getCasts() as $column => $type) {
                    if(\in_array($column, ['id', 'deleted_at', 'created_at', 'updated_at'])) {
                        continue;
                    }
                    $table->$type($column);
                }
                if(\in_array(SoftDeletes::class, Reflection::classUsesRecursive($instance))) {
                    $table->softDeletes();
                }
                if($instance->timestamps) {
                    $table->timestamps();
                }
            }
        );
    }

    private static function getSchemaBuilder(): Builder
    {
        return static::getConnection()->getSchemaBuilder();
    }

    private static function getConnection(): ConnectionInterface
    {
        return Model::getConnectionResolver()->connection();
    }

    public static function close(): void
    {
        if(static::$db && method_exists(static::$db, 'logout')) {
            static::$db->logout();
        }
    }
}
