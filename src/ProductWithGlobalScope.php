<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cetria\Laravel\Helpers\Test\Dummy\Factory\ProductFactory;

class ProductWithGlobalScope extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'products';

    protected $fillable = [
        'name',
        'url',
        'price',
    ];

    protected $casts = [
        'name' => 'string',
        'url' => 'string',
        'price' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    protected static function booted()
    {
        
        parent::booted();

        static::addGlobalScope('testGlobalScope', function(Builder $builder) {
            $builder->where('price', '<', 100);
        });
    }

    public static function factory()
    {
        return ProductFactory::new();
    }

    public function basketItems(): HasMany
    {
        return $this->hasMany(Basket::class);
    }
}
