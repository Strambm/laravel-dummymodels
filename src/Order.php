<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Cetria\Laravel\Helpers\Test\Dummy\Factory\OrderFactory;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'date',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'date' => 'date',
    ];

    public static function factory()
    {
        return OrderFactory::new();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function basketItems(): MorphMany
    {
        return $this->morphMany(Basket::class, 'object');
    }
}
