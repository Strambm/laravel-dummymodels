<?php

namespace Cetria\Laravel\Helpers\Test\Dummy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cetria\Laravel\Helpers\Test\Dummy\Factory\ProductFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'products';

    protected $fillable = [
        'name',
        'url',
        'price',
    ];

    protected $casts = [
        'name' => 'string',
        'url' => 'string',
        'price' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    public static function factory()
    {
        return ProductFactory::new();
    }

    public function basketItems(): HasMany
    {
        return $this->hasMany(Basket::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
