<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\DatabaseDrivers;

class Sqlite extends Driver
{
    protected function getConnectionData(): array
    {
        return [
            'driver' => 'sqlite',
            'database' => ':memory:',
        ];
    }
}
