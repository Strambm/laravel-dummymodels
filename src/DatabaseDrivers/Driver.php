<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\DatabaseDrivers;

use Illuminate\Database\Capsule\Manager;

abstract class Driver
{
    public function setConnection(): Manager
    {
        $db = new Manager;
        $db->addConnection($this->getConnectionData());
        $db->bootEloquent();
        $db->setAsGlobal();
        return $db;
    }

    abstract protected function getConnectionData(): array;

    public function logout(): void
    {
        
    }
}
