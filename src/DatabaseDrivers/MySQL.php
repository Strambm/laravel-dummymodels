<?php

namespace Cetria\Laravel\Helpers\Test\Dummy\DatabaseDrivers;

use Illuminate\Database\Capsule\Manager;

class MySQL extends Driver
{
    protected $host;
    protected $port;
    protected $database;
    protected $username;
    protected $password;

    protected $db;

    public function __construct(
        string $host,
        string $port,
        string $database,
        string $username,
        string $password
    )
    {
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
    }

    protected function getConnectionData(): array
    {
        return [
            'driver' => 'mysql',
            'host' => $this->host,
            'port' => $this->port,
            'database' => $this->database,
            'username' => $this->username,
            'password' => $this->password,
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
        ];
    }

    public function setConnection(): Manager
    {
        $db = parent::setConnection();
        $this->dropAllTables($db);
        $this->db = $db;
        return $db;
    }

    protected function dropAllTables(Manager $db): void
    {
        $db->getConnection()->statement('SET FOREIGN_KEY_CHECKS = 0;');
        $tables = $db->getConnection()->select('SHOW TABLES');
        foreach ($tables as $table) {
            $tableName = $table->{'Tables_in_' . $this->database};
            $db->getConnection()->statement("DROP TABLE IF EXISTS `$tableName`");
        }
        $db->getConnection()->statement('SET FOREIGN_KEY_CHECKS = 1;');
    }

    public function logout(): void
    {
        parent::logout();
        if ($this->db) {
            $this->db->getDatabaseManager()->purge();
            $this->db = null;
        }
    }
}
